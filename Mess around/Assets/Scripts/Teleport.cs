﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform target;
    public GameObject Player;

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
             Player.transform.position = target.transform.position;
             Debug.Log("Teleport");
        }
        else
        {
            Debug.Log("Tried to tele");
        }
      
    }
}
